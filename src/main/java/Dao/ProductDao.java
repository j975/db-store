/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Dao;

import Database.database;
import Model.Product;
import com.mycompany.db.store.POC.TestSelectProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product obj) {
        Connection conn = null;
        String dbPath = "./db/coffee-store.db";
        database db = database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (name, price ) VALUES (?, ?);";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            int row = stmt.executeUpdate();
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;

    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        Connection conn = null;
        database db = database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,price FROM product";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);
                System.out.println(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        database db = database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT id,name,price FROM product WHERE id=" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        String dbPath = "./db/coffee-store.db";
        database db = database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;

    }

    @Override
    public int update(Product obj) {
        Connection conn = null;
        String dbPath = "./db/coffee-store.db";
        database db = database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getId());
            row = stmt.executeUpdate();
            System.out.println("Affect row: " + row);
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {

        ProductDao dao = new ProductDao();

        System.out.println(dao.getAll());
        System.out.println(dao.get(1));

        int id = dao.add(new Product(-1, "KafaeYen", 50));
        System.out.println("id: " + id);
        System.out.println(dao.get(id));

        Product lastProduct = dao.get(id);
        System.out.println("Last Product: " + lastProduct);
        lastProduct.setPrice(40);

        Product updateProduct = dao.get(id);
        System.out.println("Update Product: " + updateProduct);

        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("Delete Product: " + deleteProduct);

    }
}
